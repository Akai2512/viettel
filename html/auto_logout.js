// Foxconn Johnson add this function on 09/04/2018
// This function is developed for Viettel's requirement
// Web GUI will auto logout without any action from user after 20 minutes

var ret=0;
var resetTime;

document.onclick = function(event){
	if(event == undefined) event=window.event;
	ret = 1;
}

function delCookie(name)
{  
	var exp = new Date();  
	exp.setTime(exp.getTime() - 10000);  
	document.cookie = name + "=del;path=/;expires=" + exp.toGMTString();  
}

function doLogout()
{
	delCookie("uid");
	delCookie("psw");
	top.location.replace("/cgi-bin/login.asp");
}

function autologout(){
	setTimeout(countClick,1000);
}

function countClick(){
	if(ret == 1){
		clearTimeout(resetTime);
		ret=0;
	}
	else{
		resetTime=setTimeout(doLogout,1200000);
	}
	setTimeout(countClick,2000);
}

//Foxconn Johnson add end
